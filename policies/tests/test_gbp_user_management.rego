package com.mercer.gbp.authz

###################################################################################
# Client Users and Groups Management
###################################################################################
# Note: Test cases are pending, and will be scoped in when impact to Authorization service will be known.

test_user_is_allowed_to_fetch_list_of_permitted_resource {
	allow with input.jwt as "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJnbG9iYWxwcm9maWxlaWQiOjQyMzc2LCJjb3JyZWxhdGlvbklkIjoiMEJReUF5NXdMcHE5cVFia2JNd1F2Um9WIiwidXNlcm5hbWUiOiJhbnRvbi5vaGl5QG1lcmNlci5jb20iLCJuYW1lSUQiOiIxMTAxMzA1Iiwic2Vzc2lvbkluZGV4IjoiX2FhZGFkNGYzLTI0MjUtNGE5NC1hMTg1LWQyZDAyM2U5NjEzYSIsImlzRmVkZXJhdGVkIjp0cnVlLCJzdWIiOjQyMzc2LCJmaXJzdE5hbWUiOiJBbnRvbiIsImxhc3ROYW1lIjoiT2hpeSIsImxhbmd1YWdlIjoiIiwicm9sZXMiOlsibW1iX29wZXJhdGlvbnMiLCJhbGxvdy5kYXRhLmNsaWVudC4xIl0sImlhdCI6MTU5NjE5NjAzOSwiZXhwIjoxNTk2MTk2MTU5LCJhdWQiOiJnYnAubWVyY2VyLmNvbSIsImlzcyI6Im1zc28ubWVyY2VyLmNvbSJ9.vRdij6hbfeotOEWgu4eaCstGND7ciZN7UPTtERocRDo"
		 with input.method as "GET"
		 with input.path as ["users", "42376", "allowed-resources"]
}
