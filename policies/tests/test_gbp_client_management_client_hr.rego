package com.mercer.gbp.authz

###################################################################################
# Client Profile and Onboarding - Role Client HR
###################################################################################
test_is_client_hr_not_allowed_to_list_all_clients {
	not allow with input.jwt as jwtClientHrRoleMissing
		 with input.method as "GET"
		 with input.path as ["countries", "gb", "gbpclients"]
}

test_is_client_hr_allowed_to_get_a_client_profile {
	allow with input.jwt as jwtClientHrGood
		 with input.method as "GET"
		 with input.path as ["countries", "gb", "gbpclients", "abc123", "profiles"]
}

test_is_client_hr_not_allowed_to_get_client_profile {
	not allow with input.jwt as jwtClientHrRoleMissing
		 with input.method as "GET"
		 with input.path as ["countries", "gb", "gbpclients", "abc123", "profiles"]
}

test_is_client_hr_allowed_to_update_a_client_profile {
	allow with input.jwt as jwtClientHrGood
		 with input.method as "PATCH"
		 with input.path as ["countries", "gb", "gbpclients", "abc123", "profiles"]
}

test_is_client_hr_not_allowed_to_update_client_profile {
	not allow with input.jwt as jwtClientHrRoleMissing
		 with input.method as "PATCH"
		 with input.path as ["countries", "gb", "gbpclients", "abc123", "profiles"]
}

test_is_client_hr_allowed_to_list_a_client_emails_domains {
	allow with input.jwt as jwtClientHrGood
		 with input.method as "GET"
		 with input.path as ["countries", "gb", "gbpclients", "abc123", "email-domains"]
}

test_is_client_hr_not_allowed_to_list_a_client_email_domains {
	not allow with input.jwt as jwtClientHrRoleMissing
		 with input.method as "GET"
		 with input.path as ["countries", "gb", "gbpclients", "abc123", "email-domains"]
}

test_is_client_hr_allowed_to_add_a_client_emails_domains {
	allow with input.jwt as jwtClientHrGood
		 with input.method as "POST"
		 with input.path as ["countries", "gb", "gbpclients", "abc123", "email-domains"]
}

test_is_client_hr_not_allowed_to_add_a_client_email_domains {
	not allow with input.jwt as jwtClientHrRoleMissing
		 with input.method as "POST"
		 with input.path as ["countries", "gb", "gbpclients", "abc123", "email-domains"]
}

test_is_client_hr_allowed_to_remove_a_client_email_domain {
	allow with input.jwt as jwtClientHrGood
		 with input.method as "DELETE"
		 with input.path as ["countries", "gb", "gbpclients", "abc123", "email-domains", "example.com"]
}

test_is_client_hr_not_allowed_to_remove_a_client_email_domain {
	not allow with input.jwt as jwtClientHrRoleMissing
		 with input.method as "DELETE"
		 with input.path as ["countries", "gb", "gbpclients", "abc123", "email-domains", "example.com"]
}

test_is_client_hr_not_allowed_to_deregister_a_client {
	not allow with input.jwt as jwtClientHrRoleMissing
		 with input.method as "DELETE"
		 with input.path as ["countries", "gb", "gbpclients", "abc123"]
}

###################################################################################
# Client Subsidiary Onboarding
###################################################################################

test_is_client_hr_allowed_to_list_all_subsidiaries_of_a_client {
	allow with input.jwt as jwtClientHrGood
		 with input.method as "GET"
		 with input.path as ["countries", "gb", "gbpclients", "abc123", "subsidiaries"]
}

test_is_client_hr_not_allowed_to_list_all_subsidiaries_of_a_client_clients {
	not allow with input.jwt as jwtClientHrRoleMissing
		 with input.method as "GET"
		 with input.path as ["countries", "gb", "gbpclients", "abc123", "subsidiaries"]
}

test_is_client_hr_allowed_to_get_a_client_subsidiary_profile {
	allow with input.jwt as jwtClientHrGood
		 with input.method as "GET"
		 with input.path as ["countries", "gb", "gbpclients", "abc123", "subsidiaries", "sub123", "profiles"]
}

test_is_client_hr_not_allowed_to_get_client_subsidiary_profile {
	not allow with input.jwt as jwtClientHrRoleMissing
		 with input.method as "GET"
		 with input.path as ["countries", "gb", "gbpclients", "abc123", "subsidiaries", "sub123", "profiles"]
}

test_is_client_hr_allowed_to_update_a_client_subsidiary_profile {
	allow with input.jwt as jwtClientHrGood
		 with input.method as "PATCH"
		 with input.path as ["countries", "gb", "gbpclients", "abc123", "subsidiaries", "sub123", "profiles"]
}

test_is_client_hr_not_allowed_to_update_client_subsidiary_profile {
	not allow with input.jwt as jwtClientHrRoleMissing
		 with input.method as "PATCH"
		 with input.path as ["countries", "gb", "gbpclients", "abc123", "subsidiaries", "sub123", "profiles"]
}

test_is_client_hr_allowed_to_list_a_client_subsidiary_emails_domains {
	allow with input.jwt as jwtClientHrGood
		 with input.method as "GET"
		 with input.path as ["countries", "gb", "gbpclients", "abc123", "subsidiaries", "sub123", "email-domains"]
}

test_is_client_hr_not_allowed_to_list_a_client_subsidiary_email_domains {
	not allow with input.jwt as jwtClientHrRoleMissing
		 with input.method as "GET"
		 with input.path as ["countries", "gb", "gbpclients", "abc123", "subsidiaries", "sub123", "email-domains"]
}

test_is_client_hr_allowed_to_add_a_client_subsidiary_emails_domains {
	allow with input.jwt as jwtClientHrGood
		 with input.method as "POST"
		 with input.path as ["countries", "gb", "gbpclients", "abc123", "subsidiaries", "sub123", "email-domains"]
}

test_is_client_hr_not_allowed_to_add_a_client_subsidiary_email_domains {
	not allow with input.jwt as jwtClientHrRoleMissing
		 with input.method as "POST"
		 with input.path as ["countries", "gb", "gbpclients", "abc123", "subsidiaries", "sub123", "email-domains"]
}

test_is_client_hr_allowed_to_remove_a_client_subsidiary_email_domain {
	allow with input.jwt as jwtClientHrGood
		 with input.method as "DELETE"
		 with input.path as ["countries", "gb", "gbpclients", "abc123", "subsidiaries", "sub123", "email-domains", "example.com"]
}

test_is_client_hr_not_allowed_to_remove_a_client_subsidiary_email_domain {
	not allow with input.jwt as jwtClientHrRoleMissing
		 with input.method as "DELETE"
		 with input.path as ["countries", "gb", "gbpclients", "abc123", "subsidiaries", "sub123", "email-domains", "example.com"]
}

test_is_client_hr_not_allowed_to_deregister_a_client_subsidiary {
	not allow with input.jwt as jwtClientHrRoleMissing
		 with input.method as "DELETE"
		 with input.path as ["countries", "gb", "gbpclients", "abc123", "subsidiaries", "sub123"]
}
