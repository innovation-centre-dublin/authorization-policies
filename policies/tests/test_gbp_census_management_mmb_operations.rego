package com.mercer.gbp.authz

###################################################################################
# HBB compatible Census Feeds, from other Systems
###################################################################################

test_is_mmb_operations_allowed_creating_feed {
	allow with input.jwt as jwtMmbOpsGood
		 with input.method as "POST"
		 with input.path as ["systems", "arcane", "census"]
}

test_is_mmb_operations_allowed_creating_feed_not {
	not allow with input.jwt as jwtMmbOpsRoleMissing
		 with input.method as "POST"
		 with input.path as ["systems", "arcane", "census"]
}

test_is_mmb_operations_not_allowed_creating_feed_for_hbb {
	not allow with input.jwt as jwtMmbOpsRoleMissing
		 with input.method as "POST"
		 with input.path as ["systems", "hbba", "census"]
}

test_is_internal_file_processor_is_allowed_creating_feed_for_hbb {
	allow with input.jwt as jwtInternalFileProcessor
		 with input.method as "POST"
		 with input.path as ["systems", "hbba", "census"]
}

test_is_internal_file_processor_not_allowed_creating_feed_for_hbbnz {
	not allow with input.jwt as jwtInternalFileProcessor
		 with input.method as "POST"
		 with input.path as ["systems", "hbbnz", "census"]
}

test_is_mmb_operations_not_allowed_deleting_feed {
	not allow with input.jwt as jwtMmbOpsGood
		 with input.method as "DELETE"
		 with input.path as ["systems", "arcane", "census", "censusid01"]
}

test_is_mmb_operations_allowed_deleting_feed_not {
	not allow with input.jwt as jwtMmbOpsRoleMissing
		 with input.method as "DELETE"
		 with input.path as ["systems", "arcane", "census", "censusid01"]
}

test_is_mmb_operations_allowed_reading_feed {
	allow with input.jwt as jwtMmbOpsGood
		 with input.method as "GET"
		 with input.path as ["systems", "arcane", "census", "censusid01"]
}

test_is_mmb_operations_allowed_reading_feed_not {
	not allow with input.jwt as jwtMmbOpsRoleMissing
		 with input.method as "GET"
		 with input.path as ["systems", "arcane", "census", "censusid01"]
}

test_is_mmb_operations_allowed_listing_feed {
	allow with input.jwt as jwtMmbOpsGood
		 with input.method as "GET"
		 with input.path as ["systems", "arcane", "census"]
}

test_is_mmb_operations_allowed_listing_feed_not {
	not allow with input.jwt as jwtMmbOpsRoleMissing
		 with input.method as "GET"
		 with input.path as ["systems", "arcane", "census"]
}

###################################################################################
# Census - Role MMB Operations
###################################################################################
test_is_mmb_operations_allowed_to_see_list_of_census {
	allow with input.jwt as jwtMmbOpsGood
		 with input.method as "GET"
		 with input.path as ["countries", "gb", "gbpclients", "abc123", "census"]
}

test_is_mmb_operations_not_allowed_to_see_list_of_census_when_not_authorized {
	not allow with input.jwt as jwtMmbOpsRoleMissing
		 with input.method as "GET"
		 with input.path as ["countries", "gb", "gbpclients", "abc123", "census"]
}

test_is_mmb_operations_not_allowed_to_see_list_of_census_when_not_authorized_for_client {
	not allow with input.jwt as jwtMmbOpsRoleMissing
		 with input.method as "GET"
		 with input.path as ["countries", "gb", "gbpclients", "clientX", "census"]
}

test_is_mmb_operations_allowed_to_see_a_particular_census_upload {
	allow with input.jwt as jwtMmbOpsGood
		 with input.method as "GET"
		 with input.path as ["countries", "gb", "gbpclients", "abc123", "census", "censusid01"]
}

test_is_mmb_operations_not_allowed_to_see_a_particular_census_upload_when_not_authorized {
	not allow with input.jwt as jwtMmbOpsRoleMissing
		 with input.method as "GET"
		 with input.path as ["countries", "gb", "gbpclients", "abc123", "census", "censusid01"]
}

test_is_mmb_operations_not_allowed_to_see_a_particular_census_upload_when_not_authorized_for_client {
	not allow with input.jwt as jwtMmbOpsGood
		 with input.method as "GET"
		 with input.path as ["countries", "gb", "gbpclients", "clientX", "census", "censusid01"]
}

test_is_mmb_operations_not_allowed_to_upload_a_particular_census_file_for_a_client_when_not_authorized {
	not allow with input.jwt as jwtMmbOpsRoleMissing
		 with input.method as "POST"
		 with input.path as ["countries", "gb", "gbpclients", "abc123", "census"]
}

test_is_mmb_operations_not_allowed_to_upload_a_particular_census_file_for_an_unauthorized_client {
	not allow with input.jwt as jwtMmbOpsGood
		 with input.method as "POST"
		 with input.path as ["countries", "gb", "gbpclients", "clientX", "census"]
}

test_is_mmb_operations_not_allowed_to_delete_a_particular_census_file_for_a_client_when_not_authorized {
	not allow with input.jwt as jwtMmbOpsRoleMissing
		 with input.method as "DELETE"
		 with input.path as ["countries", "gb", "gbpclients", "abc123", "census"]
}

test_is_mmb_operations_not_allowed_to_delete_a_particular_census_file_for_an_unauthorized_client {
	not allow with input.jwt as jwtMmbOpsGood
		 with input.method as "DELETE"
		 with input.path as ["countries", "gb", "gbpclients", "clientX", "census"]
}

###################################################################################
# Census - Member data  - MMB Ops enacting on behalf of Clients
###################################################################################
test_is_mmb_operations_allowed_to_list_members_from_any_census {
	allow with input.jwt as jwtMmbOpsGood
		 with input.method as "GET"
		 with input.path as ["countries", "gb", "gbpclients", "abc123", "census", "cen1234", "members"]
}

test_is_non_mmb_operations_not_allowed_to_list_members_from_any_census {
	not allow with input.jwt as jwtMmbOpsRoleMissing
		 with input.method as "GET"
		 with input.path as ["countries", "gb", "gbpclients", "abc123", "census", "1234", "members"]
}

test_is_mmb_operations_allowed_to_get_a_member_detail_from_any_census {
	allow with input.jwt as jwtMmbOpsGood
		 with input.method as "GET"
		 with input.path as ["countries", "gb", "gbpclients", "abc123", "census", "cen1234", "members", "member123"]
}

test_is_non_mmb_operations_not_allowed_to_get_a_member_detail_from_any_census {
	not allow with input.jwt as jwtMmbOpsRoleMissing
		 with input.method as "GET"
		 with input.path as ["countries", "gb", "gbpclients", "abc123", "census", "cen1234", "members", "member123"]
}

test_is_mmb_operations_allowed_to_list_member_dependents_from_any_census {
	allow with input.jwt as jwtMmbOpsGood
		 with input.method as "GET"
		 with input.path as ["countries", "gb", "gbpclients", "abc123", "census", "cen1234", "members", "member123", "dependants"]
}

test_is_non_mmb_operations_not_allowed_to_list_member_dependents_from_any_census {
	not allow with input.jwt as jwtMmbOpsRoleMissing
		 with input.method as "GET"
		 with input.path as ["countries", "gb", "gbpclients", "abc123", "census", "cen1234", "members", "member123", "dependants"]
}

test_is_mmb_operations_allowed_to_get_detail_of_a_member_dependent_from_any_census {
	allow with input.jwt as jwtMmbOpsGood
		 with input.method as "GET"
		 with input.path as ["countries", "gb", "gbpclients", "abc123", "census", "cen1234", "members", "member123", "dependants", "dep123"]
}

test_is_non_mmb_operations_not_allowed_to_get_detail_of_a_member_dependent_from_any_census {
	not allow with input.jwt as jwtMmbOpsRoleMissing
		 with input.method as "GET"
		 with input.path as ["countries", "gb", "gbpclients", "abc123", "census", "cen1234", "members", "member123", "dependants", "dep123"]
}

test_is_mmb_operations_allowed_to_list_members_for_client {
	allow with input.jwt as jwtMmbOpsGood
		 with input.method as "GET"
		 with input.path as ["countries", "gb", "gbpclients", "abc123", "members"]
}

test_is_non_mmb_operations_not_allowed_to_list_members_for_client {
	not allow with input.jwt as jwtMmbOpsRoleMissing
		 with input.method as "GET"
		 with input.path as ["countries", "gb", "gbpclients", "1234", "members"]
}

test_is_mmb_operations_allowed_to_get_a_member_detail_for_client {
	allow with input.jwt as jwtMmbOpsGood
		 with input.method as "GET"
		 with input.path as ["countries", "gb", "gbpclients", "abc123", "members", "member123"]
}

test_is_non_mmb_operations_not_allowed_to_get_a_member_detail_for_client {
	not allow with input.jwt as jwtMmbOpsRoleMissing
		 with input.method as "GET"
		 with input.path as ["countries", "gb", "gbpclients", "abc123", "members", "member123"]
}

test_is_mmb_operations_allowed_to_list_member_dependents_for_client {
	allow with input.jwt as jwtMmbOpsGood
		 with input.method as "GET"
		 with input.path as ["countries", "gb", "gbpclients", "abc123", "members", "member123", "dependants"]
}
