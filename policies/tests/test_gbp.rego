package com.mercer.gbp.authz

###################################################################################
# Test control data
###################################################################################
client_hr_roles := ["client_hr"]

mmb_operations_roles := ["mmb_operations", "marsh_operations", "mercer_operations"]

mmb_admin_roles := ["mmb_admin", "marsh_admin", "mercer_admin"]

#JWT tokens to support testing
jwtClientHrGood := "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJtc3NvLm1lcmNlci5jb20iLCJzdWIiOiIxMjM0NTYiLCJhdWQiOiJnYnAubWVyY2VyLmNvbSIsImp0aSI6ImEtZHVtbXktdW5pcXVlLWlkIiwibmFtZSI6Ik5lbyBBbmRlcnNvbiIsInVzZXJuYW1lIjoibmVvLmFuZGVyc29uQGFiYzEyMy5jb20iLCJnbG9iYWxwcm9maWxlaWQiOjEyMzQ1NiwiZ3JvdXBzIjpbIm1tYi1vcGVyYXRpb25zIl0sInJvbGVzIjpbImNsaWVudF9ociIsImFsbG93LmRhdGEuZ2JwY2xpZW50LmFiYzEyMyIsImFsbG93LmRhdGEuZ2JwY2xpZW50LmFiYzEyMy5zdWJzaWRpYXJ5LmFsbCIsImFsbG93LmRhdGEuY291bnRyeS5nYiIsImFsbG93LmRhdGEuY291bnRyeS5pZSJdLCJpc0ZlZGVyYXRlZCI6ZmFsc2V9.EeS_vnPEdDVdhgCTKBN5cZNhWanPOIHhOKZwWUi5ITg"

jwtClientHrRoleMissing := "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJtc3NvLm1lcmNlci5jb20iLCJzdWIiOiIxMjM0NTYiLCJhdWQiOiJnYnAubWVyY2VyLmNvbSIsImp0aSI6ImEtZHVtbXktdW5pcXVlLWlkIiwibmFtZSI6Ik5lbyBBbmRlcnNvbiIsInVzZXJuYW1lIjoibmVvLmFuZGVyc29uQGFiYzEyMy5jb20iLCJnbG9iYWxwcm9maWxlaWQiOjEyMzQ1NiwiZ3JvdXBzIjpbIm1tYi1vcGVyYXRpb25zIl0sInJvbGVzIjpbImFsbG93LmRhdGEuY2xpZW50LmFiYzEyMyIsImFsbG93LmRhdGEuY2xpZW50LmFiYzEyMy5zdWJzaWRpYXJ5LmFsbCIsImFsbG93LmRhdGEuY291bnRyeS5nYiIsImFsbG93LmRhdGEuY291bnRyeS5pZSJdLCJpc0ZlZGVyYXRlZCI6ZmFsc2V9.v0ejWgiwccMM7yH6hDdpeqnmTAQF60DH8Qq92vus4IU"

jwtMmbOpsGood := "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJtc3NvLm1lcmNlci5jb20iLCJzdWIiOiIxMjM0NTYiLCJhdWQiOiJnYnAubWVyY2VyLmNvbSIsImp0aSI6ImEtZHVtbXktdW5pcXVlLWlkIiwibmFtZSI6IkpvaG4gRG9lIiwidXNlcm5hbWUiOiJqb2huLmRvZUBtZXJjZXIuY29tIiwiZ2xvYmFscHJvZmlsZWlkIjoxMjM0NTYsImdyb3VwcyI6WyJtbWItb3BlcmF0aW9ucyJdLCJyb2xlcyI6WyJtbWJfb3BlcmF0aW9ucyIsImFsbG93LmRhdGEuZ2JwY2xpZW50LmFiYzEyMyIsImFsbG93LmRhdGEuZ2JwY2xpZW50LmFiYzIzNCIsImFsbG93LmRhdGEuY291bnRyeS5nYiIsImFsbG93LmRhdGEuY291bnRyeS5pZSIsImFsbG93LmZlZWQuY2Vuc3VzLnN5c3RlbS5hcmNhbmUiXSwiaXNGZWRlcmF0ZWQiOmZhbHNlfQ.x5NcAu1dCqlPR9oiOtNi0WUldfQkiXyRhSS0gBY_REs"

jwtMmbOpsRoleMissing := "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJtc3NvLm1lcmNlci5jb20iLCJzdWIiOiIxMjM0NTYiLCJhdWQiOiJnYnAubWVyY2VyLmNvbSIsImp0aSI6ImEtZHVtbXktdW5pcXVlLWlkIiwibmFtZSI6IkpvaG4gRG9lIiwidXNlcm5hbWUiOiJqb2huLmRvZUBtZXJjZXIuY29tIiwiZ2xvYmFscHJvZmlsZWlkIjoxMjM0NTYsImdyb3VwcyI6WyJtbWItb3BlcmF0aW9ucyJdLCJyb2xlcyI6WyJhbGxvdy5kYXRhLmNsaWVudC5hYmMxMjMiLCJhbGxvdy5kYXRhLmNsaWVudC5hYmMyMzQiLCJhbGxvdy5kYXRhLmNvdW50cnkuZ2IiLCJhbGxvdy5kYXRhLmNvdW50cnkuaWUiXSwiaXNGZWRlcmF0ZWQiOmZhbHNlfQ.TqSOQHvXsp0ZfojlCCRWIM3cCPapGMvSdDTiEQzvg5E"

jwtMmbAdminGood := "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJtc3NvLm1lcmNlci5jb20iLCJzdWIiOiIxMjM0NTYiLCJhdWQiOiJnYnAubWVyY2VyLmNvbSIsImp0aSI6ImEtZHVtbXktdW5pcXVlLWlkIiwibmFtZSI6IkpvaG4gRG9lIiwidXNlcm5hbWUiOiJqb2huLmRvZUBtZXJjZXIuY29tIiwiZ2xvYmFscHJvZmlsZWlkIjoxMjM0NTYsImdyb3VwcyI6WyJtbWItYWRtaW5zIl0sInJvbGVzIjpbIm1tYl9hZG1pbiIsImFsbG93LmRhdGEuZ2JwY2xpZW50LmFsbCIsImFsbG93LmRhdGEuY291bnRyeS5nYiIsImFsbG93LmRhdGEuY291bnRyeS5pZSJdLCJpc0ZlZGVyYXRlZCI6ZmFsc2V9.06pk-WzuEVLn4fUljDcst2sNyRcbjdENMrcN964Yo7g"

jwtMmbAdminMissing := "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJtc3NvLm1lcmNlci5jb20iLCJzdWIiOiIxMjM0NTYiLCJhdWQiOiJnYnAubWVyY2VyLmNvbSIsImp0aSI6ImEtZHVtbXktdW5pcXVlLWlkIiwibmFtZSI6IkpvaG4gRG9lIiwidXNlcm5hbWUiOiJqb2huLmRvZUBtZXJjZXIuY29tIiwiZ2xvYmFscHJvZmlsZWlkIjoxMjM0NTYsImdyb3VwcyI6WyJtbWItYWRtaW5zIl0sInJvbGVzIjpbImFsbG93LmRhdGEuY291bnRyeS5nYiIsImFsbG93LmRhdGEuY291bnRyeS5pZSJdLCJpc0ZlZGVyYXRlZCI6ZmFsc2V9.WPdNUoQC97W44JAtk0cSyFnRD-PHA0NRlkebl3QXaXE"

jwtInternalFileProcessor := "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJtc3NvLm1lcmNlci5jb20iLCJzdWIiOiJpbnRlcm5hbC1maWxlLXByb2Nlc3Nvci11cyIsImF1ZCI6ImdicC5tZXJjZXIuY29tIiwianRpIjoiYS1kdW1teS11bmlxdWUtaWQiLCJuYW1lIjoiSW50ZXJuYWwgRmlsZSBQcm9jZXNzb3IiLCJ1c2VybmFtZSI6ImpvaG4uZG9lQG1lcmNlci5jb20iLCJncm91cHMiOlsiZ2JwLWludGVybmFsLXNlcnZpY2VzIl0sInJvbGVzIjpbImFsbG93LmRhdGEuY2xpZW50LkFMTCIsImFsbG93LmRhdGEuY291bnRyeS5pZSIsImFsbG93LmZlZWQuY2Vuc3VzLnN5c3RlbS5oYmJhIl0sImlzRmVkZXJhdGVkIjpmYWxzZX0.a6Ku1zFoVsW2u1LwW1NqrqfcmPhOYjeFn7wI43SugCo"

# Generic tests to assert default behaviour

test_default_is_DENY_ALL {
	not allow with data.roles.mmb_admins as mmb_admin_roles
}

test_is_no_one_is_allowed_when_path_is_missing {
	not allow with input as {
		"user": "alice@example.com",
		"method": "GET",
		"roles": ["mmb_admin"],
	}
		 with data.roles.mmb_admins as mmb_admin_roles
}

test_is_no_one_is_allowed_when_action_is_missing {
	not allow with input as {
		"user": "alice@example.com",
		"path": ["gbpclients", "1234", "subsidiaries", "1234", "profile"],
		"roles": ["mmb_admin"],
	}
		 with data.roles.mmb_admins as mmb_admin_roles
}

test_is_mmb_admin_not_allowed_when_role_absent {
	not allow with input as {
		"user": "alice@example.com",
		"method": "GET",
		"path": ["gbpclients", "1234", "subsidiaries", "1234", "profile"],
	}
		 with data.roles.mmb_admins as mmb_admin_roles
}
