package com.mercer.gbp.authz

###################################################################################
# Census  - Role Client HR
###################################################################################
test_is_client_hr_not_allowed_to_see_list_of_census_when_not_authorized {
	not allow with input.jwt as jwtClientHrRoleMissing
		 with input.method as "GET"
		 with input.path as ["countries", "gb", "gbpclients", "abc123", "census"]
}

test_is_client_hr_not_allowed_to_see_list_of_census_when_not_authorized_for_client {
	not allow with input.jwt as jwtClientHrRoleMissing
		 with input.method as "GET"
		 with input.path as ["countries", "gb", "gbpclients", "clientX", "census"]
}

test_is_client_hr_not_allowed_to_see_a_particular_census_upload_when_not_authorized {
	not allow with input.jwt as jwtClientHrRoleMissing
		 with input.method as "GET"
		 with input.path as ["countries", "gb", "gbpclients", "abc123", "census", "censusid01"]
}

test_is_client_hr_allowed_to_see_a_latest_census_upload_when_authorized {
	allow with input.jwt as jwtClientHrGood
		 with input.method as "GET"
		 with input.path as ["countries", "gb", "gbpclients", "abc123", "census", "latest"]
}

test_is_client_hr_not_allowed_to_see_a_particular_census_upload_when_not_authorized_for_client {
	not allow with input.jwt as jwtClientHrGood
		 with input.method as "GET"
		 with input.path as ["countries", "gb", "gbpclients", "clientX", "census", "censusid01"]
}

test_is_client_hr_not_allowed_to_upload_a_particular_census_file_for_a_client_when_not_authorized {
	not allow with input.jwt as jwtClientHrRoleMissing
		 with input.method as "POST"
		 with input.path as ["countries", "gb", "gbpclients", "abc123", "census"]
}

test_is_client_hr_not_allowed_to_upload_a_particular_census_file_for_an_unauthorized_client {
	not allow with input.jwt as jwtClientHrGood
		 with input.method as "POST"
		 with input.path as ["countries", "gb", "gbpclients", "clientX", "census"]
}

test_is_client_hr_not_allowed_to_delete_a_particular_census_file_for_a_client_when_not_authorized {
	not allow with input.jwt as jwtClientHrRoleMissing
		 with input.method as "DELETE"
		 with input.path as ["countries", "gb", "gbpclients", "abc123", "census"]
}

test_is_client_hr_not_allowed_to_delete_a_particular_census_file_for_an_unauthorized_client {
	not allow with input.jwt as jwtClientHrGood
		 with input.method as "DELETE"
		 with input.path as ["countries", "gb", "gbpclients", "clientX", "census"]
}

###################################################################################
# Census - Member data  - Role Client HR
###################################################################################
test_is_client_hr_allowed_to_list_members_from_latest_census {
	allow with input.jwt as jwtClientHrGood
		 with input.method as "GET"
		 with input.path as ["countries", "gb", "gbpclients", "abc123", "census", "latest", "members"]
}

test_is_client_hr_not_allowed_to_list_members_from_non_latest_census {
	not allow with input.jwt as jwtClientHrGood
		 with input.method as "GET"
		 with input.path as ["countries", "gb", "gbpclients", "abc123", "census", "1234", "members"]
}

test_is_client_hr_allowed_to_get_a_member_detail_from_latest_census {
	allow with input.jwt as jwtClientHrGood
		 with input.method as "GET"
		 with input.path as ["countries", "gb", "gbpclients", "abc123", "census", "latest", "members", "member123"]
}

test_is_client_hr_not_allowed_to_get_a_member_detail_from_non_latest_census {
	not allow with input.jwt as jwtClientHrGood
		 with input.method as "GET"
		 with input.path as ["countries", "gb", "gbpclients", "abc123", "census", "1234", "members", "member123"]
}

test_is_client_hr_allowed_to_list_member_dependents_from_latest_census {
	allow with input.jwt as jwtClientHrGood
		 with input.method as "GET"
		 with input.path as ["countries", "gb", "gbpclients", "abc123", "census", "latest", "members", "member123", "dependants"]
}

test_is_client_hr_not_allowed_to_list_member_dependents_from_non_latest_census {
	not allow with input.jwt as jwtClientHrGood
		 with input.method as "GET"
		 with input.path as ["countries", "gb", "gbpclients", "abc123", "census", "1234", "members", "member123", "dependants"]
}

test_is_client_hr_allowed_to_get_detail_of_a_member_dependent_from_latest_census {
	allow with input.jwt as jwtClientHrGood
		 with input.method as "GET"
		 with input.path as ["countries", "gb", "gbpclients", "abc123", "census", "latest", "members", "member123", "dependants", "dep123"]
}

test_is_client_hr_not_allowed_to_get_detail_of_a_member_dependent_from_non_latest_census {
	not allow with input.jwt as jwtClientHrGood
		 with input.method as "GET"
		 with input.path as ["countries", "gb", "gbpclients", "abc123", "census", "1234", "members", "member123", "dependants", "dep123"]
}

test_is_client_hr_allowed_to_list_members_for_client {
	allow with input.jwt as jwtClientHrGood
		 with input.method as "GET"
		 with input.path as ["countries", "gb", "gbpclients", "abc123", "members"]
}

test_is_client_hr_not_allowed_to_list_members_for_client {
	not allow with input.jwt as jwtClientHrRoleMissing
		 with input.method as "GET"
		 with input.path as ["countries", "gb", "gbpclients", "1234", "members"]
}

test_is_client_hr_allowed_to_get_a_member_detail_for_client {
	allow with input.jwt as jwtClientHrGood
		 with input.method as "GET"
		 with input.path as ["countries", "gb", "gbpclients", "abc123", "members", "member123"]
}

test_is_client_hr_not_allowed_to_get_a_member_detail_for_client {
	not allow with input.jwt as jwtClientHrRoleMissing
		 with input.method as "GET"
		 with input.path as ["countries", "gb", "gbpclients", "abc123", "members", "member123"]
}

test_is_client_hr_allowed_to_list_member_dependents_for_client {
	allow with input.jwt as jwtClientHrGood
		 with input.method as "GET"
		 with input.path as ["countries", "gb", "gbpclients", "abc123", "members", "member123", "dependants"]
}

test_is_client_hr_not_allowed_to_list_member_dependents_for_client {
	not allow with input.jwt as jwtClientHrRoleMissing
		 with input.method as "GET"
		 with input.path as ["countries", "gb", "gbpclients", "abc123", "members", "member123", "dependants"]
}

test_is_client_hr_allowed_to_get_detail_of_a_member_dependent_for_client {
	allow with input.jwt as jwtClientHrGood
		 with input.method as "GET"
		 with input.path as ["countries", "gb", "gbpclients", "abc123", "members", "member123", "dependants", "dep123"]
}

test_is_client_hr_not_allowed_to_get_detail_of_a_member_dependent_for_client {
	not allow with input.jwt as jwtClientHrRoleMissing
		 with input.method as "GET"
		 with input.path as ["countries", "gb", "gbpclients", "abc123", "members", "member123", "dependants", "dep123"]
}
