package com.mercer.gbp.authz

###################################################################################
# Client Profile and Onboarding - Role MMB Admin
###################################################################################
# Register Clients across all regions
# Can a Client have the same Client Id accross various region or countries in a region???

# Allow Admin user to check presence of a Single Client and get additional details
# A private API to enquire about presence of a Client in GBP system
# Status returns identity information about client i.e. CountryId SystemOfRecordId, Mena Id and Fiduciary Id
allow {
	some countryId, sorId, fsClientId
	input.method == "GET"

	# GET /clientpresence/gb/hbb1234/fid123456/profile
	input.path = ["clientpresence", countryId, sorId, fsClientId, "profile"]
	is_mmb_admin
	is_country_data_accessible_to_user(countryId)
}

# Method to check whether GBP Id is taken or not
allow {
	input.method == "GET"
	input.path = ["checkgbpid", _, "status"]
	is_mmb_admin
}

# Allow access to List All Clients in a Country
allow {
	some countryId
	input.method == "GET"
	input.path = ["countries", countryId, "gbpclients"]
	is_mmb_admin
	is_country_data_accessible_to_user(countryId)
}

# Allow access to Client profile
allow {
	some countryId
	input.method == "GET"
	input.path = ["countries", countryId, "gbpclients", _, "profiles"]
	is_mmb_admin
	is_country_data_accessible_to_user(countryId)
}

# Allow access to Client profile
allow {
	some countryId
	input.method == "PATCH"
	input.path = ["countries", countryId, "gbpclients", _, "profiles"]
	is_mmb_admin
	is_country_data_accessible_to_user(countryId)
}

# List Email domains for a Client
allow {
	some countryId
	input.method == "GET"
	input.path = ["countries", countryId, "gbpclients", _, "email-domains"]
	is_mmb_admin
	is_country_data_accessible_to_user(countryId)
}

# Add new email domain for a client across all regions
allow {
	some countryId
	input.method == "POST"
	input.path = ["countries", countryId, "gbpclients", _, "email-domains"]
	is_mmb_admin
	is_country_data_accessible_to_user(countryId)
}

# Delete an existing email domain for a client across all regions
allow {
	some countryId
	input.method == "DELETE"
	input.path = ["countries", countryId, "gbpclients", _, "email-domains", _]
	is_mmb_admin
	is_country_data_accessible_to_user(countryId)
}

# Allow access to deregister clients from GBP
allow {
	some countryId
	input.method == "DELETE"
	input.path = ["countries", countryId, "gbpclients", _]
	is_mmb_admin
	is_country_data_accessible_to_user(countryId)
}

###################################################################################
# Client Subsidiary Onboarding
###################################################################################
# List all subsidiaries for a client
allow {
	some countryId
	input.method == "GET"
	input.path = ["countries", countryId, "gbpclients", _, "subsidiaries"]
	is_mmb_admin
	is_country_data_accessible_to_user(countryId)
}

# Read a subsidiary profile
allow {
	some countryId
	input.method == "GET"
	input.path = ["countries", countryId, "gbpclients", _, "subsidiaries", _, "profiles"]
	is_mmb_admin
	is_country_data_accessible_to_user(countryId)
}

# Update a subsidiary profile
allow {
	some countryId
	input.method == "PATCH"
	input.path = ["countries", countryId, "gbpclients", _, "subsidiaries", _, "profiles"]
	is_mmb_admin
	is_country_data_accessible_to_user(countryId)
}

# List email domains for a subsidiary
allow {
	some countryId
	input.method == "GET"
	input.path = ["countries", countryId, "gbpclients", _, "subsidiaries", _, "email-domains"]
	is_mmb_admin
	is_country_data_accessible_to_user(countryId)
}

# Add new email domain for a client across all regions
allow {
	some countryId
	input.method == "POST"
	input.path = ["countries", countryId, "gbpclients", _, "subsidiaries", _, "email-domains"]
	is_mmb_admin
	is_country_data_accessible_to_user(countryId)
}

# Delete an existing email domain for a client across all regions
allow {
	some countryId
	input.method == "DELETE"
	input.path = ["countries", countryId, "gbpclients", _, "subsidiaries", _, "email-domains", _]
	is_mmb_admin
	countryId = allowed.countries[_]
}

# Allow access to deregister a client subsidiary from GBP
allow {
	some countryId
	input.method == "DELETE"
	input.path = ["countries", countryId, "gbpclients", _, "subsidiaries", _]
	is_mmb_admin
	countryId = allowed.countries[_]
}

# Access to Subsidiary Data (Country Level) ???
allow {
	false
}

# Separate subsidiaryID ???
allow {
	false
}

# Ability to limit subs data ???
allow {
	false
}

# Add new clients(client HR can add other clientID) ???
allow {
	false
}

# Add new clients
allow {
	some countryId, gbpClientId
	input.method == "PUT"
	input.path = ["countries", countryId, "gbpclients", gbpClientId]
	is_mmb_admin
	countryId = allowed.countries[_]
}

# Question on Subsidiary
# 1. Is the subsidiary id unique within the -
#	- HBB Domain
#	- HBB Country
#	- HBB Client only
# 2. If a Client has Subsidiaries, then does all the data (Census,Policy,Invoice) are associated with the subsidiaries? Or will there be data at Client level as well.
# 3. Will the subsidiary have separate Email domain that Client, or will it be always same as the Client? Will every subsidiary have different email domain?
