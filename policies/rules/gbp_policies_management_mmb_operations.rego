package com.mercer.gbp.authz

###################################################################################
# Policy Management - Role MMB Operations
###################################################################################

# View list of Policies
allow {
	some clientId, countryId
	input.method == "GET"
	input.path = ["countries", countryId, "gbpclients", clientId, "policies"]
	is_client_data_accessible_to_user(clientId)
	is_mmb_operator
	is_country_data_accessible_to_user(countryId)
}

# View A particular Policy
allow {
	some clientId, countryId
	input.method == "GET"
	input.path = ["countries", countryId, "gbpclients", clientId, "policies", _]
	is_client_data_accessible_to_user(clientId)
	is_mmb_operator
	is_country_data_accessible_to_user(countryId)
}

# Upload particular Policy
allow {
	some clientId, countryId
	input.method == "POST"
	input.path = ["countries", countryId, "gbpclients", clientId, "policies"]
	is_client_data_accessible_to_user(clientId)
	is_mmb_operator
	is_country_data_accessible_to_user(countryId)
}

# Update contents of particular Policy
allow {
	some clientId, countryId
	input.method == "PATCH"
	input.path = ["countries", countryId, "gbpclients", clientId, "policies", _]
	is_client_data_accessible_to_user(clientId)
	is_mmb_operator
	is_country_data_accessible_to_user(countryId)
}

# Delete A particular Policy
allow {
	some clientId, countryId
	input.method == "DELETE"
	input.path = ["countries", countryId, "gbpclients", clientId, "policies", _]
	is_client_data_accessible_to_user(clientId)
	is_mmb_operator
	is_country_data_accessible_to_user(countryId)
}

###################################################################################
# Policy Attachment Management
# Attachments can be added or removed, they can never be updated
###################################################################################

# Add an attachments
allow {
	some clientId, countryId
	input.method == "POST"
	input.path = ["countries", countryId, "gbpclients", clientId, "policies", _, "documents"]
	is_client_data_accessible_to_user(clientId)
	is_mmb_operator
	is_country_data_accessible_to_user(countryId)
}

# List all attachments
allow {
	some clientId, countryId
	input.method == "GET"
	input.path = ["countries", countryId, "gbpclients", clientId, "policies", _, "documents"]
	is_client_data_accessible_to_user(clientId)
	is_mmb_operator
	is_country_data_accessible_to_user(countryId)
}

# Download an attachments
allow {
	some clientId, countryId
	input.method == "GET"
	input.path = ["countries", countryId, "gbpclients", clientId, "policies", _, "documents", _]
	is_client_data_accessible_to_user(clientId)
	is_mmb_operator
	is_country_data_accessible_to_user(countryId)
}

# Download an attachments
allow {
	some clientId, countryId
	input.method == "DELETE"
	input.path = ["countries", countryId, "gbpclients", clientId, "policies", _, "documents", _]
	is_client_data_accessible_to_user(clientId)
	is_mmb_operator
	is_country_data_accessible_to_user(countryId)
}
