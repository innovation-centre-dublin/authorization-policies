package com.mercer.gbp.authz

###################################################################################
# Invoice Management - Role MMB Operations
###################################################################################

# View list of Invoices 
allow {
	some clientId, countryId
	input.method == "GET"
	input.path = ["countries", countryId, "gbpclients", clientId, "invoices"]
	is_client_data_accessible_to_user(clientId)
	is_mmb_operator
	is_country_data_accessible_to_user(countryId)
}

# View A particular Invoice
allow {
	some clientId, countryId
	input.method == "GET"
	input.path = ["countries", countryId, "gbpclients", clientId, "invoices", _]
	is_client_data_accessible_to_user(clientId)
	is_mmb_operator
	is_country_data_accessible_to_user(countryId)
}

# Upload particular Invoice
allow {
	some clientId, countryId
	input.method == "POST"
	input.path = ["countries", countryId, "gbpclients", clientId, "invoices"]
	is_client_data_accessible_to_user(clientId)
	is_mmb_operator
	is_country_data_accessible_to_user(countryId)
}

# Update contents of particular Invoice
allow {
	some clientId, countryId
	input.method == "PATCH"
	input.path = ["countries", countryId, "gbpclients", clientId, "invoices", _]
	is_client_data_accessible_to_user(clientId)
	is_mmb_operator
	is_country_data_accessible_to_user(countryId)
}

# Delete A particular Invoice
allow {
	some clientId, countryId
	input.method == "DELETE"
	input.path = ["countries", countryId, "gbpclients", clientId, "invoices", _]
	is_client_data_accessible_to_user(clientId)
	is_mmb_operator
	is_country_data_accessible_to_user(countryId)
}

###################################################################################
# Invoice Attachment Management
# Attachments can be added or removed, they can never be updated
###################################################################################

# Add an attachments
allow {
	some clientId, countryId
	input.method == "POST"
	input.path = ["countries", countryId, "gbpclients", clientId, "invoices", _, "documents"]
	is_client_data_accessible_to_user(clientId)
	is_mmb_operator
	is_country_data_accessible_to_user(countryId)
}

# List all attachments
allow {
	some clientId, countryId
	input.method == "GET"
	input.path = ["countries", countryId, "gbpclients", clientId, "invoices", _, "documents"]
	is_client_data_accessible_to_user(clientId)
	is_mmb_operator
	is_country_data_accessible_to_user(countryId)
}

# Download an attachments
allow {
	some clientId, countryId
	input.method == "GET"
	input.path = ["countries", countryId, "gbpclients", clientId, "invoices", _, "documents", _]
	is_client_data_accessible_to_user(clientId)
	is_mmb_operator
	is_country_data_accessible_to_user(countryId)
}

# Download an attachments
allow {
	some clientId, countryId
	input.method == "DELETE"
	input.path = ["countries", countryId, "gbpclients", clientId, "invoices", _, "documents", _]
	is_client_data_accessible_to_user(clientId)
	is_mmb_operator
	is_country_data_accessible_to_user(countryId)
}
