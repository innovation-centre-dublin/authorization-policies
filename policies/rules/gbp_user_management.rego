package com.mercer.gbp.authz

###################################################################################
# Client Users and Groups Management
###################################################################################

# Allow creating a new Group for client
# Each client will have at least 2 Groups reqpresenting:
# - HR
# - Finance
# Instead of creating a higher level Role representing those business Roles, they will be represented by Group.
# This allows Roles to be Fine grained, and thus access to resources and action on them are controlled by Role.
# The Groups can be considered as Coarse grained authorization control.
# These groups will be client specific groups. And appropriate roles will be assigned to them.
allow {
	some countryId
	input.method == "PUT"
	input.path = ["countries", countryId, "gbpclients", _, "groups", _]
	is_mmb_admin
	is_country_data_accessible_to_user(countryId)
}

# Allow deleting a Group for client
allow {
	some countryId
	input.method == "DELETE"
	input.path = ["countries", countryId, "gbpclients", _, "groups", _]
	is_mmb_admin
	is_country_data_accessible_to_user(countryId)
}

# An User must be member of a Group, as Roles are assigned to a Group.
# An user can be member of more than one Group, especially when the User may be accessing more than 1 Client's data
# Allow adding User to an existing Group for client
allow {
	some clientId, clientUserId, countryId
	input.method == "PUT"
	input.path = ["countries", countryId, "gbpclients", clientId, "groups", _, "users", clientUserId]

	# Check for user email
	user_email_belongs_to_client_domain(clientId, clientUserId)
	is_mmb_admin
	is_country_data_accessible_to_user(countryId)
}

# Client HR can add user id
allow {
	some clientId, clientUserId, countryId
	input.method == "PUT"
	input.path = ["countries", countryId, "gbpclients", clientId, "groups", _, "users", clientUserId]

	# Check for user email
	user_email_belongs_to_client_domain(clientId, clientUserId)
	is_client_hr
	is_client_data_accessible_to_user(clientId)
	is_country_data_accessible_to_user(countryId)
}

# Allow existing user to be assigned to a Group belonging a Client
allow {
	some userId, clientId
	input.method == "PATCH"
	input.path = ["users", userId, "countries", countryId, "gbpclients", clientId, "groups", _]

	# Check for user email
	user_email_belongs_to_client_domain(clientId, userId)
	is_mmb_admin
}

# Every user can update their own record
allow {
	some clientUserId, countryId
	input.method == "PATCH"
	input.path = ["countries", countryId, "gbpclients", _, "groups", _, "users", clientUserId, "profile"]
	token.payload.sub == clientUserId
	is_country_data_accessible_to_user(countryId)
}

allow {
	some userId
	input.method == "PATCH"
	input.path = ["users", userId, "profile"]
	token.payload.sub == userId
}

allow {
	some userId
	input.method == "GET"
	input.path = ["users", userId, "allowed-resources"]
	format_int(token.payload.sub, 10) == userId
}

# Roles will be created during development of the Product, and they will be hardened.
# No Role will be dynamically created in the system, by the end user. Operations will only assign roles.
# A role will typically allow access to a Resouce and an Action on it.
# A role itself may group other roles, in that case it will provide access to an Union of the roles it is grouping.
# A role will only Allow access, there is no role for denial.
# By default everything is Denied.

# List of roles in a Group
allow {
	some countryId
	input.method == "GET"
	input.path = ["countries", countryId, "gbpclients", _, "groups", _, "roles"]
	is_mmb_admin
	is_country_data_accessible_to_user(countryId)
}

allow {
	some clientId, countryId
	input.method == "GET"
	input.path = ["countries", countryId, "gbpclients", clientId, "groups", _, "roles"]
	is_client_hr
	is_client_data_accessible_to_user(clientId)
	countryId = allowed.countries[_]
}

# Adding existing Role to a Group
# Only a MMB admin can do it
allow {
	some countryId
	input.method == "POST"
	input.path = ["countries", countryId, "gbpclients", _, "groups", _, "roles"]
	is_mmb_admin
	countryId = allowed.countries[_]
}

# List of users in a Group
allow {
	some countryId
	input.method == "GET"
	input.path = ["countries", countryId, "gbpclients", _, "groups", _, "users"]
	is_mmb_admin
	countryId = allowed.countries[_]
}

allow {
	some clientId, countryId
	input.method == "GET"
	input.path = ["countries", countryId, "gbpclients", clientId, "groups", _, "users"]
	is_client_hr
	is_client_data_accessible_to_user(clientId)
	countryId = allowed.countries[_]
}

# Allow removing a User for client
# Users are not deleted from the system.
allow {
	some countryId
	input.method == "DELETE"
	input.path = ["countries", countryId, "gbpclients", _, "users", _]
	is_mmb_admin
	countryId = allowed.countries[_]
}

# Client HR can remove user from their organization
allow {
	some clientId, countryId
	input.method == "DELETE"
	input.path = ["countries", countryId, "gbpclients", clientId, "users", _]
	is_client_hr
	is_client_data_accessible_to_user(clientId)
	countryId = allowed.countries[_]
}

# Allow reading all Groups of client
allow {
	some countryId
	input.method == "GET"
	input.path = ["countries", countryId, "gbpclients", _, "groups"]
	is_mmb_admin
	countryId = allowed.countries[_]
}

allow {
	some clientId, countryId
	input.method == "GET"
	input.path = ["countries", countryId, "gbpclients", clientId, "groups"]
	is_client_hr
	is_client_data_accessible_to_user(clientId)
	countryId = allowed.countries[_]
}

# Allow reading a Group of client
allow {
	some countryId
	input.method == "GET"
	input.path = ["countries", countryId, "gbpclients", _, "groups", _]
	is_mmb_admin
	countryId = allowed.countries[_]
}

allow {
	some clientId, countryId
	input.method == "GET"
	input.path = ["countries", countryId, "gbpclients", clientId, "groups", _]
	is_client_hr
	is_client_data_accessible_to_user(clientId)
	countryId = allowed.countries[_]
}

# Allow reading a User of client
allow {
	some countryId
	input.method == "GET"
	input.path = ["countries", countryId, "gbpclients", _, "users", _]
	is_mmb_admin
	countryId = allowed.countries[_]
}

allow {
	some clientId, countryId
	input.method == "GET"
	input.path = ["countries", countryId, "gbpclients", clientId, "users", _]
	is_client_hr
	is_client_data_accessible_to_user(clientId)
	countryId = allowed.countries[_]
}

allow {
	some userId
	input.method == "GET"
	input.path = ["users", userId, "allowed-resources"]
	format_int(token.payload.sub, 10) == userId
}
