package com.mercer.gbp.authz

###################################################################################
# Census - Member data  - Role Client HR
###################################################################################

# View A particular Census Data (File and Upload)
allow {
	some clientId, countryId
	input.method == "GET"
	input.path = ["countries", countryId, "gbpclients", clientId, "census", "latest"]
	is_client_hr
	is_client_data_accessible_to_user(clientId)
	is_country_data_accessible_to_user(countryId)
}

# View list of Members associated with Client, from latest Cesus feed
allow {
	some clientId, countryId
	input.method == "GET"
	input.path = ["countries", countryId, "gbpclients", clientId, "census", "latest", "members"]
	is_client_hr
	is_client_data_accessible_to_user(clientId)
	is_country_data_accessible_to_user(countryId)
}

# View details of a Member associated with Client, from latest Cesus feed
allow {
	some clientId, countryId
	input.method == "GET"
	input.path = ["countries", countryId, "gbpclients", clientId, "census", "latest", "members", _]
	is_client_hr
	is_client_data_accessible_to_user(clientId)
	is_country_data_accessible_to_user(countryId)
}

# View list of Dependents associated with a member, from latest Cesus feed
allow {
	some clientId
	input.method == "GET"
	input.path = ["countries", _, "gbpclients", clientId, "census", "latest", "members", _, "dependants"]
	is_client_hr
	is_client_data_accessible_to_user(clientId)
	countryId = allowed.countries[_]
}

# Get details of a Dependent associated with a member, from latest Cesus feed
allow {
	some clientId
	input.method == "GET"
	input.path = ["countries", _, "gbpclients", clientId, "census", "latest", "members", _, "dependants", _]
	is_client_hr
	is_client_data_accessible_to_user(clientId)
	countryId = allowed.countries[_]
}

# View list of Members associated with Client
allow {
	some clientId
	input.method == "GET"
	input.path = ["countries", _, "gbpclients", clientId, "members"]
	is_client_hr
	is_client_data_accessible_to_user(clientId)
	countryId = allowed.countries[_]
}

# View details of a Member associated with Client
allow {
	some clientId
	input.method == "GET"
	input.path = ["countries", _, "gbpclients", clientId, "members", _]
	is_client_hr
	is_client_data_accessible_to_user(clientId)
	countryId = allowed.countries[_]
}

# View list of Dependents associated with a member
allow {
	some clientId
	input.method == "GET"
	input.path = ["countries", _, "gbpclients", clientId, "members", _, "dependants"]
	is_client_hr
	is_client_data_accessible_to_user(clientId)
	countryId = allowed.countries[_]
}

# Get details of a Dependent associated with a member, from latest Cesus feed
allow {
	some clientId
	input.method == "GET"
	input.path = ["countries", _, "gbpclients", clientId, "members", _, "dependants", _]
	is_client_hr
	is_client_data_accessible_to_user(clientId)
	countryId = allowed.countries[_]
}
