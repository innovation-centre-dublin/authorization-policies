package com.mercer.gbp.authz

###################################################################################
# Policy Management - Role Client HR
###################################################################################

# View list of Policies
allow {
	some clientId, countryId
	input.method == "GET"
	input.path = ["countries", countryId, "gbpclients", clientId, "policies"]
	is_client_data_accessible_to_user(clientId)
	is_client_hr
	is_country_data_accessible_to_user(countryId)
}

# View A particular Policy
allow {
	some clientId, countryId
	input.method == "GET"
	input.path = ["countries", countryId, "gbpclients", clientId, "policies", _]
	is_client_data_accessible_to_user(clientId)
	is_client_hr
	is_country_data_accessible_to_user(countryId)
}

# List all attachments
allow {
	some clientId, countryId
	input.method == "GET"
	input.path = ["countries", countryId, "gbpclients", clientId, "policies", _, "documents"]
	is_client_data_accessible_to_user(clientId)
	is_client_hr
	is_country_data_accessible_to_user(countryId)
}

# Download an attachments
allow {
	some clientId, countryId
	input.method == "GET"
	input.path = ["countries", countryId, "gbpclients", clientId, "policies", _, "documents", _]
	is_client_data_accessible_to_user(clientId)
	is_client_hr
	is_country_data_accessible_to_user(countryId)
}
