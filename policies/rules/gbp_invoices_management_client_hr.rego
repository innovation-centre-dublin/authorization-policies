package com.mercer.gbp.authz

###################################################################################
# Invoice Management - Role Client HR
###################################################################################

# View list of Invoices 
allow {
	some clientId, countryId
	input.method == "GET"
	input.path = ["countries", countryId, "gbpclients", clientId, "invoices"]
	is_client_data_accessible_to_user(clientId)
	is_client_hr
	is_country_data_accessible_to_user(countryId)
}

# View A particular Invoice
allow {
	some clientId, countryId
	input.method == "GET"
	input.path = ["countries", countryId, "gbpclients", clientId, "invoices", _]
	is_client_data_accessible_to_user(clientId)
	is_client_hr
	is_country_data_accessible_to_user(countryId)
}

# List all attachments
allow {
	some clientId, countryId
	input.method == "GET"
	input.path = ["countries", countryId, "gbpclients", clientId, "invoices", _, "documents"]
	is_client_data_accessible_to_user(clientId)
	is_client_hr
	is_country_data_accessible_to_user(countryId)
}

# Download an attachments
allow {
	some clientId, countryId
	input.method == "GET"
	input.path = ["countries", countryId, "gbpclients", clientId, "invoices", _, "documents", _]
	is_client_data_accessible_to_user(clientId)
	is_client_hr
	countryId = allowed.countries[_]
}
