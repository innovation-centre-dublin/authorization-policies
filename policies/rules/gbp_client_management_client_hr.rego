package com.mercer.gbp.authz

###################################################################################
# Client Profile and Onboarding - Role Client HR
###################################################################################

# Allow access to Client profile
allow {
	some clientId, countryId
	input.method == "GET"
	input.path = ["countries", countryId, "gbpclients", clientId, "profiles"]
	is_client_hr
	is_client_data_accessible_to_user(clientId)
	is_country_data_accessible_to_user(countryId)
}

# Allow access to Client profile
allow {
	some clientId, countryId
	input.method == "PATCH"
	input.path = ["countries", countryId, "gbpclients", clientId, "profiles"]
	is_client_hr
	is_client_data_accessible_to_user(clientId)
	is_country_data_accessible_to_user(countryId)
}

# List emails associated with client
allow {
	some clientId, countryId
	input.method == "GET"
	input.path = ["countries", countryId, "gbpclients", clientId, "email-domains"]
	is_client_hr
	is_client_data_accessible_to_user(clientId)
	is_country_data_accessible_to_user(countryId)
}

# Add new email domain for a client across all regions
allow {
	some clientId, countryId
	input.method == "POST"
	input.path = ["countries", countryId, "gbpclients", clientId, "email-domains"]
	is_client_hr
	is_client_data_accessible_to_user(clientId)
	is_country_data_accessible_to_user(countryId)
}

# Delete an existing email domain for a client across all regions
allow {
	some clientId, countryId
	input.method == "DELETE"
	input.path = ["countries", countryId, "gbpclients", clientId, "email-domains", _]
	is_client_hr
	is_client_data_accessible_to_user(clientId)
	is_country_data_accessible_to_user(countryId)
}

###################################################################################
# Client Subsidiary Onboarding
###################################################################################
# List all subsidiaries for a client
allow {
	some clientId, countryId
	input.method == "GET"
	input.path = ["countries", countryId, "gbpclients", clientId, "subsidiaries"]
	is_client_hr
	is_client_data_accessible_to_user(clientId)
	is_country_data_accessible_to_user(countryId)
}

# Read a new subsidiary profile
allow {
	some clientId, countryId
	input.method == "GET"
	input.path = ["countries", countryId, "gbpclients", clientId, "subsidiaries", _, "profiles"]
	is_client_hr
	is_client_data_accessible_to_user(clientId)
	is_country_data_accessible_to_user(countryId)
}

# Update a new subsidiary profile
allow {
	some clientId, countryId
	input.method == "PATCH"
	input.path = ["countries", countryId, "gbpclients", clientId, "subsidiaries", _, "profiles"]
	is_client_hr
	is_client_data_accessible_to_user(clientId)
	is_country_data_accessible_to_user(countryId)
}

# List emails associated with client subsidiary
allow {
	some clientId, countryId
	input.method == "GET"
	input.path = ["countries", countryId, "gbpclients", clientId, "subsidiaries", _, "email-domains"]
	is_client_hr
	is_client_data_accessible_to_user(clientId)
	is_country_data_accessible_to_user(countryId)
}

# Add new email domain for a client across all regions
allow {
	some clientId, countryId
	input.method == "POST"
	input.path = ["countries", countryId, "gbpclients", clientId, "subsidiaries", _, "email-domains"]
	is_client_hr
	is_client_data_accessible_to_user(clientId)
	is_country_data_accessible_to_user(countryId)
}

# Delete an existing email domain for a client across all regions
allow {
	some clientId, countryId
	input.method == "DELETE"
	input.path = ["countries", countryId, "gbpclients", clientId, "subsidiaries", _, "email-domains", _]
	is_client_hr
	is_client_data_accessible_to_user(clientId)
	is_country_data_accessible_to_user(countryId)
}
