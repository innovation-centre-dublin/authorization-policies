package com.mercer.gbp.authz

######################################################################################
# Control Data
######################################################################################
control_data := {
	"coarse_roles": {
		"mmb_admins": ["mmb_admin", "marsh_admin", "mercer_admin"],
		"mmb_operations": ["mmb_operations", "marsh_operations", "mercer_operations"],
		"client_hr": ["client_hr"],
		"client_finance": ["client_finance"],
		"client_subsidiary_hr": ["client_subsidiary_hr"],
		"client_subsidiary_finance": ["client_subsidiary_finance"],
	},
	"supported_feeds": ["census", "policy", "invoice"],
}

######################################################################################
# Control Domain Data
######################################################################################
# By default do not allow anyone access to GBP
default allow = false

allowed := {
	"gbpclients": [client | ["allow", "data", "gbpclient", client] = split(token.payload.roles[_], ".")],
	"countries": [country | ["allow", "data", "country", country] = split(token.payload.roles[_], ".")],
	"feeds": {{"feed": feed, "systems": systems} |
		feed := control_data.supported_feeds[_]
		systems := [system | ["allow", "feed", feed, "system", system] = split(token.payload.roles[_], ".")]
	},
}

######################################################################################
# Helper Functions
######################################################################################
# Function to check whether the user role is MMB Admin
is_mmb_admin {
	token.payload.roles[_] == control_data.coarse_roles.mmb_admins[_]
}

# Function to check whether the user role is MMB Ops
is_mmb_operator {
	token.payload.roles[_] == control_data.coarse_roles.mmb_operations[_]
}

# Function to check whether the user role is Client HR
is_client_hr {
	token.payload.roles[_] == control_data.coarse_roles.client_hr[_]
}

# Function to check Subsidiary HR
contains_client_subsidiary_hr(roles) {
	roles[_] == control_data.coarse_roles.client_subsidiary_hr[_]
}

# Function to check Client Finance
contains_client_finance(roles) {
	roles[_] == control_data.coarse_roles.client_finance[_]
}

# Function to check Subsidiary Finance
contains_client_subsidiary_finance(roles) {
	roles[_] == control_data.coarse_roles.client_subsidiary_finance[_]
}

# Function to check whether a User belongs to a Client domain
user_email_belongs_to_client_domain(clientId, clientUserId) {
	clientUserEmailDomain := user_email_domain(clientUserId)

	# Does email domain exist in the approved list of client email domains
	data.clients[clienId].email_domains[clientUserEmailDomain]
}

# Helper function to check the provided data is potentially an email address
# Note: convert it to a regex based pattern
user_email_domain(userId) {
	arr = split(userId, "@")
	arr[minus(count(arr), 1)]
}

# Helper function to decode JWT
# JWT is expected to follow the structure mentioned in the example below:
# {
#   "iss": "msso.mercer.com",
#   "sub": "123456",
#   "aud": "gbp.mercer.com",
#   "jti": "a-dummy-unique-id",
#   "first_name": "John",
#   "last_name": "Samurai Doe",
#   "username": "john.doe@mercer.com",
#   "globalprofileid": 123456,
#   "groups": [
#     "mmb-operations"
#   ],
#   "roles": [
#     "mmb_operations",
#     "allow.data.gbpclient.MINAabc123",
#     "allow.data.gbpclient.MONEabc234",
#     "allow.data.country.gb",
#     "allow.data.country.ie",
#     "allow.feed.billing.system.hbb_nz",
#     "allow.feed.census.system.arcane",
#     "allow.feed.census.system.hbb_nz",
#     "allow.feed.policy.system.hbb_nz",
#     "allow.feed.invoice.system.hbb_au"
#   ],
#   "isFederated": false
# }
#
token = {"payload": payload} {
	[header, payload, signature] := io.jwt.decode(input.jwt)
}

# Helper function to test whether an User is allowed access to a particular Client
is_client_data_accessible_to_user(clientId) {
	clientId = allowed.gbpclients[_]
}

# Helper function to test whether an User is allowed access to a particular Client
is_country_data_accessible_to_user(country) {
	county = allowed.countries[_]
}

# Helper function to test whether an User is allowed access to a particular system
is_feed_for_system_allowed(feedType, systemId) {
	feedType = allowed.feeds[_].feed
	systemId = allowed.feeds[_].systems[_]
}
