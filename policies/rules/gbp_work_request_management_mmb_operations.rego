package com.mercer.gbp.authz

###################################################################################
# Workrequest
###################################################################################
# View A Workrequest
allow {
	some clientId, countryId
	input.method == "GET"
	input.path = ["countries", countryId, "gbpclients", clientId, "workrequests", _]
	clientId = allowed.gbpclients[_]
	is_mmb_operator
	is_country_data_accessible_to_user(countryId)
}

# View list of documents attached to a Workrequest
allow {
	some clientId, countryId
	input.method == "GET"
	input.path = ["countries", countryId, "gbpclients", clientId, "workrequests", _, "documents"]
	clientId = allowed.gbpclients[_]
	is_mmb_operator
	is_country_data_accessible_to_user(countryId)
}

# View a document attached to a Workrequest
allow {
	some clientId, countryId
	input.method == "GET"
	input.path = ["countries", countryId, "gbpclients", clientId, "workrequests", _, "documents", _]
	clientId = allowed.gbpclients[_]
	is_mmb_operator
	is_country_data_accessible_to_user(countryId)
}

# Create A Workrequest
allow {
	some clientId, countryId
	input.method == "POST"
	input.path = ["countries", countryId, "gbpclients", clientId, "workrequests"]
	clientId = allowed.gbpclients[_]
	is_mmb_operator
	is_country_data_accessible_to_user(countryId)
}

# Update A Workrequest, i.e. add comments
# As details on fields that can be updated, is it limited to Comments
allow {
	some clientId, countryId
	input.method == "POST"
	input.path = ["countries", countryId, "gbpclients", clientId, "workrequests", _, "comments"]
	clientId = allowed.gbpclients[_]
	is_mmb_operator
	is_country_data_accessible_to_user(countryId)
}

# Update the status of a WorkRequest
allow {
	some clientId, countryId
	input.method == "PATCH"
	input.path = ["countries", countryId, "gbpclients", clientId, "workrequests", _, "status"]
	clientId = allowed.gbpclients[_]
	is_mmb_operator
	countryId = allowed.countries[_]
}

# Attach a document to workrequest
allow {
	some clientId, countryId
	input.method == "POST"
	input.path = ["countries", countryId, "gbpclients", clientId, "workrequests", _, "documents"]
	clientId = allowed.gbpclients[_]
	is_mmb_operator
	countryId = allowed.countries[_]
}

# Delete a document on workrequest
allow {
	some clientId, countryId
	input.method == "DELETE"
	input.path = ["countries", countryId, "gbpclients", clientId, "workrequests", _, "documents", _]
	clientId = allowed.gbpclients[_]
	is_mmb_operator
	countryId = allowed.countries[_]
}
