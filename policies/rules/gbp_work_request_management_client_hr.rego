package com.mercer.gbp.authz

###################################################################################
# Workrequest
###################################################################################
# View A Workrequest
allow {
	some clientId, countryId
	input.method == "GET"
	input.path = ["countries", countryId, "gbpclients", clientId, "workrequests", _]
	is_client_data_accessible_to_user(clientId)
	is_client_hr
	is_country_data_accessible_to_user(countryId)
}

# View list of documents attached to a Workrequest
allow {
	some clientId, countryId
	input.method == "GET"
	input.path = ["countries", countryId, "gbpclients", clientId, "workrequests", _, "documents"]
	is_client_data_accessible_to_user(clientId)
	is_client_hr
	is_country_data_accessible_to_user(countryId)
}

# View a document attached to a Workrequest
allow {
	some clientId, countryId
	input.method == "GET"
	input.path = ["countries", countryId, "gbpclients", clientId, "workrequests", _, "documents", _]
	is_client_data_accessible_to_user(clientId)
	is_client_hr
	is_country_data_accessible_to_user(countryId)
}

# Create A Workrequest
allow {
	some clientId, countryId
	input.method == "POST"
	input.path = ["countries", countryId, "gbpclients", clientId, "workrequests"]
	is_client_data_accessible_to_user(clientId)
	is_client_hr
	is_country_data_accessible_to_user(countryId)
}
