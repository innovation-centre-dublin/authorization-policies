package com.mercer.gbp.authz

###################################################################################
# Reports Management - Role MMB Operations
# Presently all reports are uploaded as attachments, so body of the report will be more of lesss metadata of Report.
# Actual content will be inform of the attachment.
###################################################################################

# View list of Reports
allow {
	some clientId, countryId
	input.method == "GET"
	input.path = ["countries", countryId, "gbpclients", clientId, "reports"]
	is_client_data_accessible_to_user(clientId)
	is_mmb_operator
	is_country_data_accessible_to_user(countryId)
}

# View A particular Report
allow {
	some clientId, countryId
	input.method == "GET"
	input.path = ["countries", countryId, "gbpclients", clientId, "reports", _]
	is_client_data_accessible_to_user(clientId)
	is_mmb_operator
	is_country_data_accessible_to_user(countryId)
}

# Upload particular Report
allow {
	some clientId, countryId
	input.method == "POST"
	input.path = ["countries", countryId, "gbpclients", clientId, "reports"]
	is_client_data_accessible_to_user(clientId)
	is_mmb_operator
	is_country_data_accessible_to_user(countryId)
}

# Update contents of particular Report
allow {
	some clientId, countryId
	input.method == "PATCH"
	input.path = ["countries", countryId, "gbpclients", clientId, "reports", _]
	is_client_data_accessible_to_user(clientId)
	is_mmb_operator
	is_country_data_accessible_to_user(countryId)
}

# Delete A particular Report
allow {
	some clientId, countryId
	input.method == "DELETE"
	input.path = ["countries", countryId, "gbpclients", clientId, "reports", _]
	is_client_data_accessible_to_user(clientId)
	is_mmb_operator
	is_country_data_accessible_to_user(countryId)
}

###################################################################################
# Report Attachment Management
# Attachments can be added or removed, they can never be updated
###################################################################################

# Add an attachments
allow {
	some clientId, countryId
	input.method == "POST"
	input.path = ["countries", countryId, "gbpclients", clientId, "reports", _, "documents"]
	is_client_data_accessible_to_user(clientId)
	is_mmb_operator
	is_country_data_accessible_to_user(countryId)
}

# List all attachments
allow {
	some clientId, countryId
	input.method == "GET"
	input.path = ["countries", countryId, "gbpclients", clientId, "reports", _, "documents"]
	is_client_data_accessible_to_user(clientId)
	is_mmb_operator
	is_country_data_accessible_to_user(countryId)
}

# Download an attachments
allow {
	some clientId, countryId
	input.method == "GET"
	input.path = ["countries", countryId, "gbpclients", clientId, "reports", _, "documents", _]
	is_client_data_accessible_to_user(clientId)
	is_mmb_operator
	is_country_data_accessible_to_user(countryId)
}

# Download an attachments
allow {
	some clientId, countryId
	input.method == "DELETE"
	input.path = ["countries", countryId, "gbpclients", clientId, "reports", _, "documents", _]
	is_client_data_accessible_to_user(clientId)
	is_mmb_operator
	is_country_data_accessible_to_user(countryId)
}
