package com.mercer.gbp.authz

###################################################################################
# Reports Management - Role Client Finance
###################################################################################

# View list of Reports
allow {
	some clientId, countryId
	input.method == "GET"
	input.path = ["countries", countryId, "gbpclients", clientId, "reports"]
	is_client_data_accessible_to_user(clientId)
	contains_client_finance(input.roles)
	is_country_data_accessible_to_user(countryId)
}

# View A particular Report
allow {
	some clientId, countryId
	input.method == "GET"
	input.path = ["countries", countryId, "gbpclients", clientId, "reports", _]
	is_client_data_accessible_to_user(clientId)
	contains_client_finance(input.roles)
	is_country_data_accessible_to_user(countryId)
}

# List all attachments
allow {
	some clientId, countryId
	input.method == "GET"
	input.path = ["countries", countryId, "gbpclients", clientId, "reports", _, "documents"]
	is_client_data_accessible_to_user(clientId)
	contains_client_finance(input.roles)
	is_country_data_accessible_to_user(countryId)
}

# Download an attachments
allow {
	some clientId, countryId
	input.method == "GET"
	input.path = ["countries", countryId, "gbpclients", clientId, "reports", _, "documents", _]
	is_client_data_accessible_to_user(clientId)
	contains_client_finance(input.roles)
	is_country_data_accessible_to_user(countryId)
}
