package com.mercer.gbp.authz

###################################################################################
# HBB compatible Census Feeds, from other Systems
###################################################################################
# Allow uploading of a Census feed from a HBB or non-HBB system
allow {
	some systemId
	input.method == "POST"
	input.path = ["systems", systemId, "census"]

	# The user must be assigned a role that allows them to upload census feed from a particular system.
	# e.g. create.feeds.census.system.mysystem
	# Where "mysystem" is the system generating the feed.
	is_feed_for_system_allowed("census", systemId)
}

# Allow seeing of all Census feeds from a HBB or non-HBB system
allow {
	some systemId
	input.method == "GET"
	input.path = ["systems", systemId, "census"]

	# The user must be assigned a role that allows them to upload census feed from a particular system.
	# e.g. create.feeds.census.system.mysystem
	# Where "mysystem" is the system generating the feed.
	is_feed_for_system_allowed("census", systemId)
}

# Allow seeing a particular Census feeds from a HBB or non-HBB system
allow {
	some systemId
	input.method == "GET"
	input.path = ["systems", systemId, "census", _]

	# The user must be assigned a role that allows them to upload census feed from a particular system.
	# e.g. create.feeds.census.system.mysystem
	# Where "mysystem" is the system generating the feed.
	is_feed_for_system_allowed("census", systemId)
}

###################################################################################
# Census - Role MMB Operations - where they act on behalf of Client
###################################################################################

# View list of Census Data (File and Upload)
allow {
	some clientId, countryId
	input.method == "GET"
	input.path = ["countries", countryId, "gbpclients", clientId, "census"]
	is_mmb_operator
	is_client_data_accessible_to_user(clientId)
	is_country_data_accessible_to_user(countryId)
}

# View A particular Census Data (File and Upload)
allow {
	some clientId, countryId
	input.method == "GET"
	input.path = ["countries", countryId, "gbpclients", clientId, "census", _]
	is_mmb_operator
	is_client_data_accessible_to_user(clientId)
	countryId = allowed.countries[_]
}

# View list of Members associated with Client, from latest Cesus feed
allow {
	some clientId, countryId
	input.method == "GET"
	input.path = ["countries", countryId, "gbpclients", clientId, "census", _, "members"]
	is_mmb_operator
	is_client_data_accessible_to_user(clientId)
	countryId = allowed.countries[_]
}

# View details of a Member associated with Client, from latest Cesus feed
allow {
	some clientId, countryId
	input.method == "GET"
	input.path = ["countries", countryId, "gbpclients", clientId, "census", _, "members", _]
	is_mmb_operator
	is_client_data_accessible_to_user(clientId)
	countryId = allowed.countries[_]
}

# View list of Dependents associated with a member, from latest Cesus feed
allow {
	some clientId, countryId
	input.method == "GET"
	input.path = ["countries", countryId, "gbpclients", clientId, "census", _, "members", _, "dependants"]
	is_mmb_operator
	is_client_data_accessible_to_user(clientId)
	countryId = allowed.countries[_]
}

# Get details of a Dependent associated with a member, from latest Cesus feed
allow {
	some clientId, countryId
	input.method == "GET"
	input.path = ["countries", countryId, "gbpclients", clientId, "census", _, "members", _, "dependants", _]
	is_mmb_operator
	is_client_data_accessible_to_user(clientId)
	countryId = allowed.countries[_]
}

# View list of Members associated with Client
allow {
	some clientId, countryId
	input.method == "GET"
	input.path = ["countries", countryId, "gbpclients", clientId, "members"]
	is_mmb_operator
	is_client_data_accessible_to_user(clientId)
	countryId = allowed.countries[_]
}

# View details of a Member associated with Client
allow {
	some clientId, countryId
	input.method == "GET"
	input.path = ["countries", countryId, "gbpclients", clientId, "members", _]
	is_mmb_operator
	is_client_data_accessible_to_user(clientId)
	countryId = allowed.countries[_]
}

# View list of Dependents associated with a member
allow {
	some clientId, countryId
	input.method == "GET"
	input.path = ["countries", countryId, "gbpclients", clientId, "members", _, "dependants"]
	is_mmb_operator
	is_client_data_accessible_to_user(clientId)
	countryId = allowed.countries[_]
}

# Get details of a Dependent associated with a member, from latest Cesus feed
allow {
	some clientId, countryId
	input.method == "GET"
	input.path = ["countries", countryId, "gbpclients", clientId, "members", _, "dependants", _]
	is_mmb_operator
	is_client_data_accessible_to_user(clientId)
	countryId = allowed.countries[_]
}
