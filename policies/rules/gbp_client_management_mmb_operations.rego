package com.mercer.gbp.authz

###################################################################################
# Client Profile and Onboarding
###################################################################################

# Allow access to Client profile
allow {
	some clientId, countryId
	input.method == "GET"
	input.path = ["countries", countryId, "gbpclients", clientId, "profile"]
	is_client_data_accessible_to_user(clientId)
	is_mmb_operator
	is_country_data_accessible_to_user(countryId)
}

# Allow access to Client profile
allow {
	some clientId, countryId
	input.method == "PATCH"
	input.path = ["countries", countryId, "gbpclients", clientId, "profile"]
	is_client_data_accessible_to_user(clientId)
	is_mmb_operator
	is_country_data_accessible_to_user(countryId)
}

# Add new email domain for a client across all regions
allow {
	some clientId, countryId
	input.method == "POST"
	input.path = ["countries", countryId, "gbpclients", clientId, "email_domains"]
	is_client_data_accessible_to_user(clientId)
	is_mmb_operator
	is_country_data_accessible_to_user(countryId)
}

# Update an existing email domain for a client across all regions
allow {
	some clientId, countryId
	input.method == "PATCH"
	input.path = ["countries", countryId, "gbpclients", clientId, "email_domains", _]
	is_client_data_accessible_to_user(clientId)
	is_mmb_operator
	is_country_data_accessible_to_user(countryId)
}

# Delete an existing email domain for a client across all regions
allow {
	some clientId, countryId
	input.method == "DELETE"
	input.path = ["countries", countryId, "gbpclients", clientId, "email_domains", _]
	is_client_data_accessible_to_user(clientId)
	is_mmb_operator
	is_country_data_accessible_to_user(countryId)
}

###################################################################################
# Client Subsidiary Onboarding
###################################################################################

# Read a new subsidiary profile
allow {
	some clientId, countryId
	input.method == "GET"
	input.path = ["countries", countryId, "gbpclients", clientId, "subsidiaries", _, "profile"]
	is_client_data_accessible_to_user(clientId)
	is_mmb_operator
	is_country_data_accessible_to_user(countryId)
}

# Update a new subsidiary profile
allow {
	some clientId, countryId
	input.method == "PATCH"
	input.path = ["countries", countryId, "gbpclients", clientId, "subsidiaries", _, "profile"]
	is_client_data_accessible_to_user(clientId)
	is_mmb_operator
	is_country_data_accessible_to_user(countryId)
}

# Add new email domain for a client across all regions
allow {
	some clientId, countryId
	input.method == "POST"
	input.path = ["countries", countryId, "gbpclients", clientId, "subsidiaries", _, "email_domains"]
	is_client_data_accessible_to_user(clientId)
	is_mmb_operator
	is_country_data_accessible_to_user(countryId)
}

# Update an existing email domain for a client across all regions
allow {
	some clientId, countryId
	input.method == "PATCH"
	input.path = ["countries", countryId, "gbpclients", clientId, "subsidiaries", _, "email_domains", _]
	is_client_data_accessible_to_user(clientId)
	is_mmb_operator
	is_country_data_accessible_to_user(countryId)
}

# Delete an existing email domain for a client across all regions
allow {
	some clientId, countryId
	input.method == "DELETE"
	input.path = ["countries", countryId, "gbpclients", clientId, "subsidiaries", _, "email_domains", _]
	is_client_data_accessible_to_user(clientId)
	is_mmb_operator
	is_country_data_accessible_to_user(countryId)
}
