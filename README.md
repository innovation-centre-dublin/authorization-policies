# Health GBP - Authorization Policies

## Access Policies
OPA engine will be responsible for evaluating the policies associated with the users. The policies will be available in rego file format, and will be created during the introduction of a feature in the GBP.

Where a policy is absent for a feature, the default behaviour will be **Deny** during policy evaluation.

The user context (i.e. JWT token) and control data must be passed the OPA engine for evaluation. By default, all the policies and data that OPA uses to make decisions are kept in-memory. OPA is designed to enable distributed policy enforcement.

All the policies associated with GBP will be managed in a git repository that's separate from application code repository. Thus ensuring that more suitable governance can be applied to updates on policies.

> **Note:** The release/deployment of the Policy to OPA can be done in various ways. It need to be discussed further with Mercer architects and security to select the best approach.  Only one approach is considered, as of now, as dominantly MercerOs applications are build using NodeJs. A separate approach, i.e. NodeJs agnostic approach, will be taken in OSS2 based platform.

##### Policy Publishing to OPA
```mermaid
graph LR

subgraph Accessing Policies
J(Jenkins) --read Policies--> BB(Bitbucket/Git Repository)
end

subgraph  Publishing Policies
J --publishes Policies --> N(Nexus Respository)
end
```


##### Policy Evaluation at OPA
```mermaid
graph LR

subgraph Requesting Policies Evaluation
MA(Main API Service) --policy evaluation--> OPA(OPA Service)
MA --provides--> input>"Input (with JWT)"]
MA --provides-->control>"Control Data (Conditional)"]
OPA --access--> Policies[(Policy Cache)]
OPA --access--> input
OPA --access--> control
end
```

## Compiling and Publishing the Policies
OPA compiler allows the policies to be published as `WASM`.

> [WebAssembly (abbreviated Wasm)](https://webassembly.org/) is a binary instruction format for a stack-based virtual machine. Wasm is designed as a portable compilation target for programming languages, enabling deployment on the web for a client and server applications.

OPA is able to compile Rego policies into executable Wasm modules that can be evaluated with different inputs and external data. This is not running the OPA server in Wasm, nor is this just cross-compiled Golang code. The compiled Wasm module is a planned evaluation path for the source policy and query.

### Installing OPA compiler
Follow the instruction at [Open Policy Agent](https://www.openpolicyagent.org/docs/latest/#running-opa) official site to download and prepare the enviornment for `opa`.

### Formatting the Policies

Execute the following command for formatting the OPA policies, and ensuring that they conform to standard format.
```
opa fmt -w policies/
```
OR
```
cd npm
npm run-script format
```
These will rewrite all the existing policy files and apply correct formatting.

### Testing

Execute the following command for Testing the OPA policies.
```
opa test -v policies/rules/* policies/tests/*
```
OR
```
cd dist
npm test
```
This should build the `wasm` file and output a zipped tar file `bundle.tar.gz`. 

### Compiling

Execute the following command for compiling the OPA policies.
```
opa build -t wasm -o dist/bundle.tar.gz -e com/mercer/gbp/authz policies/rules/*
```
OR
```
cd dist 
npm run-script compile
```
This should build the `wasm` file and output a zipped tar file `bundle.tar.gz`. 

### Publishing
Execute the following command to publish the `wasm`
```
cd dist
npm publish
```
